//este archivo es el fichero fuente que al compilarse produce el ejecutable Ej1
#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <sys/msg.h>
#include <sys/ipc.h>
#include <signal.h>
#include <sys/wait.h>
#include <sys/times.h>
#include <string.h>
//Macros
#define MAX 30

//FUNCIONES, prototipos
void writeOnPipe(int fd[2]);
char* getPipeMessage(int fd[2]);
void createsFIFOnamedfichero1();
void writeOnFIFO(char *message);
int createsMessageQueue();
pid_t getPIDFromMessageQueue(int msgid);
//estructura para el mensaje
typedef struct myMessageType {
	long mtype;
	pid_t pid;
}MessageStruct;

int main() {
	//PARA LA MEDICION DE LOS TIEMPOS
	struct tms pb1,pb2;
	clock_t time1,time2;
	int CLK_TCK  = sysconf(_SC_CLK_TCK);
	//FIN VARIABLES Y STRUCTURAS MDICION TIEMPOS
    int fd[2];//vector que contendra los descriptores de la tuberia, 0 lee,1 escribe 
    pipe(fd);//Creamos la tuberia
	int pid = fork();

    //Ejecucion del padre , devolvera el pid de P1
    if(pid > 0) {
		time1 = times(&pb1);//Tomamos el tiempo inicial
        writeOnPipe(fd);
        int msgid = createsMessageQueue();
		pid_t p3_pid = getPIDFromMessageQueue(msgid);
		//Envia señal a sigterm a p3 
		kill(p3_pid,SIGTERM);
		unlink("./fichero1");
		printf("El proceso P1 (PID: %d, Ej1). Ha enviado una señal SIGTERM al proceso cuyo pid es %d , y eliminado el fichero1 del sistema de ficheros.\n",getpid(),p3_pid);
		wait(NULL);//en este hilo de ejecucion pid sera el PID HIJO
		//Mostrar estadisticas
		time2 = times(&pb2);//tiempo final
		float parentUserTime = (float)(pb2.tms_utime - pb1.tms_utime /CLK_TCK);
		float parentSystemTime = (float)(pb2.tms_stime - pb1.tms_stime /CLK_TCK);
		float childsUserTime = (float)(pb2.tms_cutime - pb1.tms_cutime /CLK_TCK);
		float childsSystemTime = (float)(pb2.tms_cstime - pb1.tms_cstime /CLK_TCK);
		
		printf("\nTiempo proceso padre modo nucleo: %g \n",parentSystemTime);
		printf("\nTiempo proceso padre modo usuario: %g \n",parentUserTime);
		printf("\nTiempo hijos modo nucleo: %g \n",childsSystemTime);
		printf("\nTiempo hijos modo usuario: %g \n",childsUserTime);
    }
    //Ejecucion del hijo,devolvera un 0, P2
    else if(pid == 0) {
        char * messsage = getPipeMessage(fd);
        createsFIFOnamedfichero1();
        writeOnFIFO(messsage);
        execl("./Ej2","./",NULL);
    }
    //Error
    else {
        perror("Error al crear el proceso hijo.\n");
        exit(EXIT_FAILURE);
    }
    return EXIT_SUCCESS;
}

//Esta funcion toma el descriptor de escritura de una tuberia.
void writeOnPipe(int fd[2]) {
	char writeBuffer[MAX];
	//Pedimos el mensaje
	printf("Escribe el mensaje que será enviado: ");
	fgets(writeBuffer,MAX,stdin);
    close(fd[0]);
	//Lo escribimos
	if(write(fd[1],writeBuffer,sizeof(char)*MAX) > 0) {
		printf("El proceso P1 (PID: %d, Ej1).Transmite un mensaje al proceso P2 por una tubería sin nombre.\n",getpid());
		close(fd[1]);//Cerramos el lado de escritura
	}else{
		perror("No se ha podido escribir en al tuberia.\n");
		exit(EXIT_FAILURE);
	}	
}
//Funcion para leer el mensaje de una tuberia a la que le pasamos el descriptro de lectura
char* getPipeMessage(int fd[2]) {
	char *readBuffer = malloc(MAX * sizeof(char));
	close(fd[1]);
	if(read(fd[0],readBuffer,sizeof(char)*MAX) > 0) {
		close(fd[0]);//Cerramos el lado de lectura
		printf("El proceso P2 (PID: %i, Ej1).Recibe un mensaje del proceso P1 por una tubería sin nombre. Contenido: %s\n",getpid(),readBuffer);
	}else{
		perror("No se ha podido leer la tuberia.\n");
		exit(EXIT_FAILURE);
	}
	return readBuffer;
}

////////////// FUNCIONES FIFO /////////////////

void createsFIFOnamedfichero1() {
	if(mknod("fichero1",S_IFIFO|0666,0) == -1) {
		perror("No se ha podido crear la tuberia FIFO.\n");
		exit(EXIT_FAILURE);
	}else {
		printf("El proceso P2 (PID: %i, Ej1). Crea un fichero FIFO de nombre fichero1\n",getpid());
	}	
}

void writeOnFIFO(char *message) {

	int fdfifoWriter = open("./fichero1", O_RDWR );
	
	if(fdfifoWriter != -1 && (write(fdfifoWriter,message,sizeof(char)*MAX)) > 0) {
		printf("El proceso P2 (PID: %i, Ej1). Ha escrito el mensaje en el fichero1.\n",getpid());
        //close(fdfifoWriter);
	}else {
		perror("No se ha podido abrir el fichero FIFO.\n");
		exit(EXIT_FAILURE);
	}
}

///////////////////FUNCIONES COLA DE MENSAJES /////////////////
int createsMessageQueue() {
	key_t key = ftok("Ej1",1993);
	int msgid = msgget(key,0666|IPC_CREAT);
	
	if((key != -1) && (msgid != -1)) {
		printf("El proceso P1 (PID: %d, Ej1) ha creado una cola de mensajes asociada a Ej1.\n",getpid());
	}else {
		perror("No se ha podido crear la cola de mensajes.\n");
		exit(EXIT_FAILURE);
	}
	return msgid;
}

pid_t getPIDFromMessageQueue(int msgid) {
	MessageStruct message;
	int result = msgrcv(msgid,(struct MessageStruct *)&message,sizeof(message.pid),0,MSG_NOERROR);
	if(result == -1) {
		perror("Ha ocurrido un problema al leer la respuesta de la cola de mensajes.\n");
		exit(EXIT_FAILURE);
	}else {
		printf("El proceso P1 (PID: %i, Ej1) ha recibido un mensaje por la cola de mensajes. Contiene: %i\n",getpid(),message.pid);
        msgctl (msgid, IPC_RMID, (struct msqid_ds *)NULL);//Cerramos la cola dem enasjes
        return message.pid;
	}
}
