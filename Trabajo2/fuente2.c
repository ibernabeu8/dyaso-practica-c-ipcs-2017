//este archivo es el fichero fuente que al compilarse produce el ejecutable Ej2
#include <stdio.h>
#include <fcntl.h>
#include <unistd.h>
#include <string.h>
#include <sys/ipc.h>
#include <sys/types.h>
#include <stdlib.h>
#include <sys/sem.h>
#include <sys/shm.h>
#include <string.h>
#include <wait.h>

//Prototipos
char* readFIFO();
char* createSharedMemoryRegion();
int createSem();
struct sembuf V();
//
#define MAX 30
//
int main(int argc, char *argv[]) {

    char *message;
	char *vc1;
	int sem1;

	message = readFIFO();
    vc1 = createSharedMemoryRegion();//Nos devuelve el puntero a la zona de memoria, aqui guardaremos la palabra que hemos leido del fifo
	sem1 = createSem();

    //Ahora tenemos que proteger esa region con un semaforo inicilizamos el semaforo:
	semctl(sem1,0,SETVAL,-1); //INICIALIZA EN ROJO
	//definimos la operacion para abrir el semaforo

	//escribimos el mensaje en la region de memoria
	int pid = fork();

	if(pid > 0) { //padre
		sleep(1);
		//escribir en la variable compartida v1
		strcpy(vc1,message);
		shmdt((void *) vc1);//
		printf("El proceso P2 (PID: %i, Ej2) ha escrito el mensaje en la variable compartida vc1.\n",getpid());
		//abrir el semaforo
        struct sembuf Operation = V();
		semop(sem1,&Operation,1);
        //se quedara esperando a que termine su hijo, p3
		printf("El proceso P2 (PID: %i, Ej2) quedará a la espera de que acabe su hijo p3.\n",getpid());
        wait(NULL);
		printf("El proceso P2 (PID: %i, Ej2) ha terminado su espera.\n",getpid());
	}else if (pid == 0) {//hijo
		execl("./Ej3","./",NULL);
	}else {
		perror("Error creando proceso hijo.");
		exit(EXIT_FAILURE);
	}
	return EXIT_SUCCESS;
}

/////////////////////LECTURA FIFO
char* readFIFO() {
	char *readBuffer = malloc(MAX * sizeof(char));
	int fdreader = open("./fichero1", O_RDWR);

	if(fdreader != -1 && (read(fdreader,readBuffer,sizeof(char)*MAX)) > 0) {
		printf("El proceso P2 (PID: %i, Ej2) ha leido el mensaje de la tuberia FIFO. Mensaje: %s\n",getpid(),readBuffer);
        close(fdreader);
	}else {
		perror("No se ha podido abrir el FIFO.");
		exit(EXIT_FAILURE);	
	}
	return readBuffer;
}

//Crea una region de memoria compartida y devuelve un puntero a la zona
char* createSharedMemoryRegion() {
	key_t key = ftok("./fichero1",1993);
	int shmid = shmget (key, MAX*sizeof(char), 0777 | IPC_CREAT);
	
	if (shmid != -1 ) {
		printf("El proceso P2 (PID: %i, Ej2) ha creado una región de memoria compartida asociada a fichero1.\n",getpid());
	}else {
		perror("No se ha podido crear la región de memoria.");
		exit(EXIT_FAILURE);
	}
	//Creamos un puntero a dicha zona
	return shmat(shmid,0,0);
}
//crea un conjunto de semaforos y devuelve el identficador, en este caso solo se va a crear 1 unico semaforo
int createSem() {
	key_t key = ftok("./fichero1",1993);
	int semid = semget(key,1,IPC_CREAT|0600); //2º parametro numero de semaforos
	
	if(semid != -1) {
		printf("El proceso P2 (PID: %i, Ej2) ha creado un semaforo asociados a fichero1.\n",getpid());
	}else {
		perror("No se ha podido crear el conjunto de semáforos.");
		exit(EXIT_FAILURE);
	}
	return semid;
}

//
struct sembuf V() {
    struct sembuf Operation;
    Operation.sem_num = 0; //seleccionamos el semaforo que queremos en este caso : Semaforo 1
	Operation.sem_op = 1; //para abrirlo.
	Operation.sem_flg = 0;
	printf("El proceso P2 (PID: %i, Ej2) va a proceder a abrir el semaforo asociado a fichero1.\n",getpid());
    return Operation;
}