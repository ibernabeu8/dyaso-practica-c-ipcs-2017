#include <stdio.h>
#include <sys/ipc.h>
#include <sys/shm.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/sem.h>
#include <string.h>
#include <sys/msg.h>
#include <time.h>
#include <mqueue.h>
#include <signal.h>
//
#define MAX 30
//prototipos
char* openSharedMemory();
int openSem();
int openMessageQueue();
void sentPIDthrowMSG(int);
struct sembuf P();
//void makeTime();
void waitTime (unsigned int);
//
typedef struct myMessageType {
	long mtype;
	pid_t pid;
}MessageStruct;

//
int main() {
	
	char* ptr_vc1, *message;
	int sem1,msgid;
	struct sembuf Operation;

	//makeTime();
	ptr_vc1 = openSharedMemory(); //Abrimos la memoria compartida, obtenemos un puntero
	sem1 = openSem(); //Obtenemos el semaforo
	
    //Cerramos el semaforo, hasta que P2 lo abra lo cual significara que ya esta escrito el mensaje, asi evitamos no leer nada
    Operation = P();
	semop(sem1,&Operation,1);//P()

	//Cuando este abierto obtenemos el mensaje de la region compartida
	message = malloc(MAX*sizeof(char));
	int i = 0;
	while(ptr_vc1[i] != '\0') {
		message[i] = ptr_vc1[i];
		i++;
	}
	shmdt((void *) ptr_vc1);//liberamos recursos, el puntero q va a la memoria compartida
    printf("El proceso P3 (PID: %i, Ej3) lee el siguiente mensaje: %s\n",getpid(),message);
    //Realiza una espera de 1 segundo aproximadamente
	printf("El proceso P3 (PID: %i, Ej3) procede a realizar una espera de 1 segundo aproximadamente.\n",getpid());
    waitTime(1);
	//Abrimos la cola de mensajes
	msgid = openMessageQueue();
	sentPIDthrowMSG(msgid);
	close(msgid);
	close(sem1);
	printf("El proceso P3 (PID: %i, Ej3) entra en pausa a la espera de señales.\n",getpid());
	pause();
	//Cerramos recursos
	//sem_close(sem1);
	//return EXIT_SUCCESS;
}

char* openSharedMemory() {
	key_t key = ftok("./fichero1",1993);
	
	int shmid = shmget(key, MAX*sizeof(char), 0777);
	
	if(shmid != -1) {
		printf("El proceso P3 (PID: %i, Ej3) ha cargado correctamente la memoria compartida asociada a fichero1.\n",getpid());
	}else {
		perror("No se ha podido cargar la memoria compartida");
		exit(EXIT_FAILURE);
	}
	return shmat(shmid,0,0);
}

int openSem() {
	key_t key = ftok("./fichero1",1993);
	int semid = semget(key,1,0600); //2º parametro numero de semaforos
	
	if(semid != -1) {
		printf("El proceso P3 (PID: %i, Ej3) ha cargado el semaforo asociados a fichero1.\n",getpid());
	}else {
		perror("No se ha podido cargar el semáforo.");
		exit(EXIT_FAILURE);
	}
	return semid;
}

int openMessageQueue() {
	key_t key = ftok("Ej1",1993);
	int msgid = msgget(key,0666);
	
	if((key != -1) && (msgid != -1)) {
		printf("El proceso P3 (PID: %d, Ej3) ha creado una cola de mensajes asociada a Ej1.\n",getpid());
	}else {
		perror("No se ha podido crear la cola de mensajes.\n");
		exit(EXIT_FAILURE);
	}
	return msgid;
}

void sentPIDthrowMSG(int msgid) {
	
	MessageStruct message;
	
	message.mtype = 1;
	message.pid = getpid();

	int result = msgsnd (msgid,(struct MessageStruct *)&message, sizeof(message.mtype) + sizeof(message.pid),0);
	
	if(result != -1) {
		printf("El proceso P3 (PID: %d, Ej3) ha enviado su PID al proceso P1 en Ej1.\n",getpid());
	}else {
		perror("No se ha podido enviar el mensaje.");
		exit(EXIT_FAILURE);
	}
}

struct sembuf P() {
    struct sembuf Operation;
	//operacion para cerrar el semaforo
	Operation.sem_num = 0; //Semaforo 1
	Operation.sem_op = -1; //P()
    Operation.sem_flg = 0;
    printf("El proceso P3 (PID: %d, Ej3) va a cerrar el semaforo asociado a fichero1.\n",getpid());
    return Operation;
}

void waitTime (unsigned int secs) {
    unsigned int relativeTime = time(0) + secs;   // Tiempo final
    while (time(0) < relativeTime);               // Este bucle no hace nada, solo sirve para gastar tiempo de CPU de manera activa, a diferencia del sleep.
}
